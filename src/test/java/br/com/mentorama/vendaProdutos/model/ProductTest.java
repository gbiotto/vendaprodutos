package br.com.mentorama.vendaProdutos.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    @Test
    public void shouldCalculateTotalProceWithDiscount(){
        Product product = new Product("Banana", 50.0,0.10,30);
        Double result = product.getPriceWithDiscount(0.10);
        assertEquals(45.0, result);
    }

    @Test
    public void whenDiscountIsHigherThenMaxDiscountShouldUseMaxDiscountPercentage(){
        Product product = new Product("Banana", 50.0,0.10,30);
        Double result = product.getPriceWithDiscount(0.15);
        assertEquals(45.0, result);
    }

    @Test
    public void whenDiscountIsLowerThenMaxDiscountShouldUsePrpvidedDiscount(){
        Product product = new Product("Banana", 50.0,0.10,30);
        Double result = product.getPriceWithDiscount(0.05);
        assertEquals(47.5, result);
    }

}