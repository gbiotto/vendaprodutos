package br.com.mentorama.vendaProdutos.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderItemTest {

    @Test
    public void shouldCalculateTotalPrice() {
        final Product product = new Product("Celular", 600.0, 0.04, 1);
        OrderItem orderItem = new OrderItem(product, 2, 0.05);
        final Double result = orderItem.totalPrice();

        assertEquals(576.0, result);
    }

}