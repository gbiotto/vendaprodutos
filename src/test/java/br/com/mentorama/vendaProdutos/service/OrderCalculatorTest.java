package br.com.mentorama.vendaProdutos.service;

import br.com.mentorama.vendaProdutos.model.Order;
import br.com.mentorama.vendaProdutos.model.OrderItem;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.*;

class OrderCalculatorTest {

    private final OrderCalculator orderCalculator;

    public OrderCalculatorTest(){
        System.out.println("Construtor");
        this.orderCalculator = new OrderCalculator();
    }

    @Test
    public void shouldCalculateTotalOrderPrice(){
        final Order order = new Order(aListOfOrderItem());
        Double result = orderCalculator.calculateOrder(order);
        assertEquals(40, result);
    }

    @Test
    public void shouldCalculateTotalOfMultipleOrders(){
        final List<Order> orders =
                Arrays.asList(
                        new Order(aListOfOrderItem()),
                        new Order(aListOfOrderItem())
                );
        final Double result = orderCalculator.calculateMultipleOrders(orders);
        assertEquals(80.0, result);
    }

    private List<OrderItem> aListOfOrderItem(){
        return Arrays.asList(
                aOrderItem(30.0),
                aOrderItem(10.0)
        );
    }

    private OrderItem aOrderItem(final Double expectedValue){
        OrderItem orderItem = mock(OrderItem.class);
        when(orderItem.totalPrice()).thenReturn(expectedValue);
        return orderItem;
    }

}