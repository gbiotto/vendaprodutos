package br.com.mentorama.vendaProdutos.clients;

import br.com.mentorama.vendaProdutos.model.Product;
import br.com.mentorama.vendaProdutos.model.ProductDTO;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ProductClienteAPI {

    @Value("${vendaProdutos.url}")
    private String url;

    @RolesAllowed("user")
    public List<Product> findAll(){
        ResponseEntity<ProductDTO> responseEntity =
                new RestTemplate().getForEntity(url, ProductDTO.class);
        return responseEntity.getBody().getProducts();
    }

    @RolesAllowed("user")
    public List<Product> findPrice(){
        ResponseEntity<ProductDTO> responseEntity =
                new RestTemplate().getForEntity(url, ProductDTO.class);
        return responseEntity.getBody().getProducts();
    }
}
