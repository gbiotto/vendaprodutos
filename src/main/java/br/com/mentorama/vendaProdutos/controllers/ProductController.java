package br.com.mentorama.vendaProdutos.controllers;

import br.com.mentorama.vendaProdutos.model.OrderItem;
import br.com.mentorama.vendaProdutos.model.Product;
import br.com.mentorama.vendaProdutos.service.ProductService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/venda")
public class ProductController {

    private List<Product> products;

    private OrderItem orderItem;

    public void Product(){
        this.products = new ArrayList<>();
    }

    @Autowired
    private ProductService productService;

    @RolesAllowed("user")
    @GetMapping
    public List<Product> findAll(){
        return productService.findAll();
    }
    public List<Product> findPrice(){
        return productService.findPrice();
    }

    @RolesAllowed("admin")
    @PostMapping
    public ResponseEntity<Integer> add(@RequestBody Product product){
        if(product.getId() == null && orderItem.getQuantityRequest() == null){
            System.out.println("Falha ao inserir produto");
        }
        products.add(product);
        return new ResponseEntity<>(product.getId(), HttpStatus.CREATED);
    }
}
