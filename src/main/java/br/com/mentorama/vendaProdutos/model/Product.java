package br.com.mentorama.vendaProdutos.model;

import java.util.List;

public class Product {

    private String name;
    private Integer id = 0;
    private Double price;
    private Double maxDiscount;
    private Integer quantityAvailable;

    public Product(String name, Double price, Double maxDiscount, Integer quantityAvailable) {
        this.name = name;
        this.id += 1;
        this.price = price;
        this.maxDiscount = maxDiscount;
        this.quantityAvailable = quantityAvailable;
    }

    public Double getPriceWithDiscount(final Double discount){
        if(discount > maxDiscount){
            return price * (1 - maxDiscount);
        }else{
            return price * (1 - discount);
        }
    }

    public Integer getQuantityAvailable(Integer quantityRequest){
        if (quantityAvailable > quantityRequest){
            return this.quantityAvailable - quantityRequest;
        }else{
            return quantityRequest = this.quantityAvailable;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(Double maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    public Integer getQuantityAvailable() {
        return quantityAvailable;
    }

    public void setQuantityAvailable(Integer quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }
}
