package br.com.mentorama.vendaProdutos.model;

public class OrderItem {

    private Product product;
    private Integer quantityRequest;
    private Double discount;

    public OrderItem(Product product, Integer quantity, Double discount) {
        this.product = product;
        this.quantityRequest = quantity;
        this.discount = discount;
    }

    public Double totalPrice(){
        if (product.getQuantityAvailable() > quantityRequest ){
            return product.getPriceWithDiscount(discount) * quantityRequest;
        }else{
            return product.getPriceWithDiscount(discount) * product.getQuantityAvailable();
        }
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantityRequest() {
        return quantityRequest;
    }

    public void setQuantityRequest(Integer quantityRequest) {
        this.quantityRequest = quantityRequest;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }
}
