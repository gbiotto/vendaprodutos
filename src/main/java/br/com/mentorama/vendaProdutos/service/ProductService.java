package br.com.mentorama.vendaProdutos.service;

import br.com.mentorama.vendaProdutos.clients.ProductClienteAPI;
import br.com.mentorama.vendaProdutos.model.Product;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    private ProductClienteAPI client;

    public List<Product> findAll(){
        return client.findAll();
    }

    public List<Product> findPrice(){
        return client.findPrice();
    }

}
